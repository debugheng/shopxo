<?php
// +----------------------------------------------------------------------
// | ShopXO 国内领先企业级B2C免费开源电商系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2019 http://shopxo.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Devil
// +----------------------------------------------------------------------
namespace app\index\controller;

use app\service\ArticleService;
use app\service\CommonTagService;
use app\service\SeoService;

/**
 * 文章详情
 * @author   Devil
 * @blog     http://gong.gg/
 * @version  0.0.1
 * @datetime 2016-12-01T21:51:08+0800
 */
class Article extends Common
{
	/**
     * 构造方法
     * @author   Devil
     * @blog    http://gong.gg/
     * @version 1.0.0
     * @date    2018-11-30
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();
    }

	/**
     * [Index 文章详情]
     * @author   Devil
     * @blog     http://gong.gg/
     * @version  0.0.1
     * @datetime 2016-12-06T21:31:53+0800
     */
	public function Index()
	{
		// 获取文章
		$id = input('id');
		$params = [
			'where' => ['a.is_enable'=>1, 'a.id'=>$id],
			'field' => 'a.id,a.title,a.title_color,a.jump_url,a.content,a.access_count,a.article_category_id,seo_title,seo_keywords,seo_desc,a.add_time',
			'm' => 0,
			'n' => 1,
		];
		$article = ArticleService::ArticleList($params);
		if(!empty($article['data'][0]))
		{
			// 访问统计
			ArticleService::ArticleAccessCountInc(['id'=>$id]);

			// 是否外部链接
			if(!empty($article['data'][0]['jump_url']))
			{
				return redirect($article['data'][0]['jump_url']);
			}

			// 获取分类和文字
			$article_category_content = ArticleService::ArticleCategoryListContent();
            $this->assign('category_list', $article_category_content['data']);

            // seo
            $seo_title = empty($article['data'][0]['seo_title']) ? $article['data'][0]['title'] : $article['data'][0]['seo_title'];
            $this->assign('home_seo_site_title', SeoService::BrowserSeoTitle($seo_title, 2));
            if(!empty($article['data'][0]['seo_keywords']))
            {
                $this->assign('home_seo_site_keywords', $article['data'][0]['seo_keywords']);
            }
            if(!empty($article['data'][0]['seo_desc']))
            {
                $this->assign('home_seo_site_description', $article['data'][0]['seo_desc']);
            }

			$this->assign('article', $article['data'][0]);
			return $this->fetch();
		} else {
			$this->assign('msg', '文章不存在或已删除');
			return $this->fetch('public/tips_error');
		}
	}

	public function newslist(){
        $cid = input('cid');

        // 分页
        $number = 10;

        $params['article_category_id'] = $cid;
        $params['page'] = input('page');

        // 条件
        $where = ArticleService::ArticleListWhere($params);

        // 获取总数
        $total = ArticleService::ArticleTotal($where);

        // 分页
        $page_params = array(
            'number'    =>  $number,
            'total'     =>  $total,
            'where'     =>  $params,
            'page'      =>  isset($params['page']) ? intval($params['page']) : 1,
            'url'       =>  MyUrl('index/article/newslist', ['cid' => $cid]),
        );
        $page = new \base\Page($page_params);
        $this->assign('page_html', $page->GetPageHtml2());

        // 获取列表
        $data_params = array(
            'm'     => $page->GetPageStarNumber(),
            'n'     => $number,
            'where' => $where,
            'field' => 'a.*',
        );
        $data = ArticleService::ArticleList($data_params);

        //当前分类
        $current_category = ArticleService::getCategoryDetail($cid);

        $this->assign('category', $current_category);

        $this->assign('data_list', $data['data']);

        $this->assign('category_id', $cid);

        $cat_where = ['id' => [10,25,26,27,28]];
        $category_list = ArticleService::ArticleCategoryList(['field'=>'id,name', 'conditions' => $cat_where]);
        $this->assign('category_list', $category_list);

        $category_article_list = $this->getCatArticleList($category_list['data']);
        $this->assign('category_article_list', $category_article_list);

        $this->assign('home_seo_site_title', SeoService::BrowserSeoTitle('新闻中心', 1));


        // 获取列表
        $tag_data_params = array(
            'm'     => 0,
            'n'     => 30,
            'where' => $where,
            'field' => '*',
        );
        $tag_data = CommonTagService::TagList($tag_data_params);
        $this->assign('tag_data_list', $tag_data['data']);
        return $this->fetch();
    }

    protected function getCatArticleList($cats){
        $params = [
            'where' => [],
            'field' => 'a.id,a.title,a.title_color,ac.name AS category_name, ac.alias_name',
            'm' => 0,
            'n' => 4,
        ];

        foreach ($cats as &$cat){
            $article_where = ['article_category_id' => $cat['id']];
            $params['where'] = $article_where;
            $article_list = ArticleService::ArticleList($params);
            $cat['article_list'] = $article_list['data'];
        }

        return $cats;
    }

    public function newstag(){
        $tag_id = input('tag_id');
        // 分页
        $number = 10;
        $params['tag_id'] = $tag_id;
        $params['page'] = input('page');

        // 条件
        $where = ArticleService::ArticleListWhere($params);

        // 获取总数
        $total = ArticleService::ArticleTotal($where);

        // 分页
        $page_params = array(
            'number'    =>  $number,
            'total'     =>  $total,
            'where'     =>  $params,
            'page'      =>  isset($params['page']) ? intval($params['page']) : 1,
            'url'       =>  MyUrl('index/article/newstag', ['tag_id' => $tag_id]),
        );
        $page = new \base\Page($page_params);
        $this->assign('page_html', $page->GetPageHtml2());

        // 获取列表
        $data_params = array(
            'm'     => $page->GetPageStarNumber(),
            'n'     => $number,
            'where' => $where,
            'field' => 'a.*',
        );
        $data = ArticleService::ArticleList($data_params);

        //当前分类
        $current_tag = CommonTagService::getTagDetail($tag_id);

        $this->assign('tag', $current_tag);

        $this->assign('data_list', $data['data']);

        $cat_where = ['id' => [10,25,26,27,28]];
        $category_list = ArticleService::ArticleCategoryList(['field'=>'id,name', 'conditions' => $cat_where]);
        $this->assign('category_list', $category_list);

        $category_article_list = $this->getCatArticleList($category_list['data']);
        $this->assign('category_article_list', $category_article_list);

        $tag_data_params = array(
            'm'     => 0,
            'n'     => 30,
            'where' => $where,
            'field' => '*',
        );
        $tag_data = CommonTagService::TagList($tag_data_params);
        $this->assign('tag_data_list', $tag_data['data']);

        return $this->fetch('article/tag/list');
    }
}
?>