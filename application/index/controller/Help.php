<?php
// +----------------------------------------------------------------------
// | ShopXO 国内领先企业级B2C免费开源电商系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2019 http://shopxo.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Devil
// +----------------------------------------------------------------------
namespace app\index\controller;

use app\service\HelpService;
use app\service\SeoService;

/**
 * 帮助中心-文章详情
 * @author   Devil
 * @blog     http://gong.gg/
 * @version  0.0.1
 * @datetime 2016-12-01T21:51:08+0800
 */
class Help extends Common
{
    /**
     * 构造方法
     * @author   Devil
     * @blog    http://gong.gg/
     * @version 1.0.0
     * @date    2018-11-30
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * [Index 文章详情]
     * @author   Devil
     * @blog     http://gong.gg/
     * @version  0.0.1
     * @datetime 2016-12-06T21:31:53+0800
     */
    public function Index()
    {

        $this->assign('category_list', HelpService::categoryAll(['field'=>'id,name']));
        $this->assign('home_seo_site_title', SeoService::BrowserSeoTitle('帮助中心', 1));
        return $this->fetch();
    }

    public function helplist(){
        // 参数
        $params = input();


        // 分页
        $number = MyC('admin_page_number', 10, true);

        $params['is_more'] = 1;
        // 条件
        $where = HelpService::ArticleListWhere($params);

        // 获取总数
        $total = HelpService::ArticleTotal($where);

        // 分页
        $page_params = array(
            'number'    =>  $number,
            'total'     =>  $total,
            'where'     =>  $params,
            'page'      =>  isset($params['page']) ? intval($params['page']) : 1,
            'url'       =>  MyUrl('help/helplist'),
        );
        $page = new \base\Page($page_params);
        $this->assign('page_html', $page->GetPageHtml());

        // 获取列表
        $data_params = array(
            'm'     => $page->GetPageStarNumber(),
            'n'     => $number,
            'where' => $where,
            'field' => 'a.*',
        );
        $data = HelpService::ArticleList($data_params);

        //当前分类
        $current_category = HelpService::getCategoryDetail($params['category_id']);

        $this->assign('category', $current_category);

        $this->assign('data_list', $data['data']);

        $this->assign('category_id', $params['category_id']);

        $this->assign('category_list', HelpService::categoryAll(['field'=>'id,name']));
        $this->assign('home_seo_site_title', SeoService::BrowserSeoTitle('帮助中心', 1));
        return $this->fetch();
    }

    public function Detail(){
        // 获取文章
        $id = input('id');
        $params = [
            'where' => ['a.is_enable'=>1, 'a.id'=>$id],
            'field' => 'a.id,a.title,a.title_color,a.jump_url,a.content,a.access_count,a.category_id,seo_title,seo_keywords,seo_desc,a.add_time',
            'm' => 0,
            'n' => 1,
        ];
        $article = HelpService::ArticleList($params);
        if(!empty($article['data'][0]))
        {
            // 访问统计
            HelpService::ArticleAccessCountInc(['id'=>$id]);

            // 是否外部链接
            if(!empty($article['data'][0]['jump_url']))
            {
                return redirect($article['data'][0]['jump_url']);
            }

            // 获取分类和文字
            $article_category_content = HelpService::HelpCategoryListContent();
            $this->assign('category_list', $article_category_content['data']);

            // seo
            $seo_title = empty($article['data'][0]['seo_title']) ? $article['data'][0]['title'] : $article['data'][0]['seo_title'];
            $this->assign('home_seo_site_title', SeoService::BrowserSeoTitle($seo_title, 2));
            if(!empty($article['data'][0]['seo_keywords']))
            {
                $this->assign('home_seo_site_keywords', $article['data'][0]['seo_keywords']);
            }
            if(!empty($article['data'][0]['seo_desc']))
            {
                $this->assign('home_seo_site_description', $article['data'][0]['seo_desc']);
            }

            $this->assign('article', $article['data'][0]);
            $this->assign('category_list', HelpService::categoryAll(['field'=>'id,name']));

            // 获取列表
            $data_params = array(
                'where' => [
                    ['a.category_id', '=', $article['data'][0]['category_id']],
                    ['a.id', '<>', $article['data'][0]['id']],
                ],
                'field' => 'a.*',
            );
            $relation_data = HelpService::ArticleList($data_params);
            $this->assign('relation_article', $relation_data['data']);

            $this->assign('category_id', $article['data'][0]['category_id']);

            $current_category = HelpService::getCategoryDetail($article['data'][0]['category_id']);
//            var_dump($current_category);exit;
            $this->assign('category', $current_category);

            return $this->fetch();
        } else {
            $this->assign('msg', '文章不存在或已删除');
            return $this->fetch('public/tips_error');
        }
    }

    public function search(){
        // 参数
        $params = input();

        // 分页
        $number = MyC('admin_page_number', 10, true);

        $params['is_more'] = 1;
        // 条件
        $where = HelpService::ArticleListWhere($params);

        // 获取总数
        $total = HelpService::ArticleTotal($where);

        // 分页
        $page_params = array(
            'number'    =>  $number,
            'total'     =>  $total,
            'where'     =>  $params,
            'page'      =>  isset($params['page']) ? intval($params['page']) : 1,
            'url'       =>  MyUrl('help/helplist'),
        );
        $page = new \base\Page($page_params);
        $this->assign('page_html', $page->GetPageHtml());

        // 获取列表
        $data_params = array(
            'm'     => $page->GetPageStarNumber(),
            'n'     => $number,
            'where' => $where,
            'field' => 'a.*',
        );
        $data = HelpService::ArticleList($data_params);

        $this->assign('data_list', $data['data']);

        $this->assign('keywords', $params['keywords']);

        $this->assign('category_list', HelpService::categoryAll(['field'=>'id,name']));
        $this->assign('home_seo_site_title', SeoService::BrowserSeoTitle('帮助中心--'.$params['keywords'], 3));
        return $this->fetch();
    }
}
?>