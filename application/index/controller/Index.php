<?php
// +----------------------------------------------------------------------
// | ShopXO 国内领先企业级B2C免费开源电商系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2019 http://shopxo.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Devil
// +----------------------------------------------------------------------
namespace app\index\controller;

use app\service\CustomerService;
use app\service\HelpService;
use think\facade\Hook;
use app\service\BannerService;
use app\service\GoodsService;
use app\service\ArticleService;
use app\service\OrderService;
use app\service\AppHomeNavService;

/**
 * 首页
 * @author   Devil
 * @blog     http://gong.gg/
 * @version  0.0.1
 * @datetime 2016-12-01T21:51:08+0800
 */
class Index extends Common
{
    /**
     * 构造方法
     * @author   Devil
     * @blog    http://gong.gg/
     * @version 1.0.0
     * @date    2018-11-30
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 首页
     * @author   Devil
     * @blog     http://gong.gg/
     * @version  1.0.0
     * @datetime 2018-12-02T11:11:49+0800
     */
    public function Index()
    {
        // 首页轮播
        $this->assign('banner_list', BannerService::Banner());

        // H5导航
        $this->assign('navigation', AppHomeNavService::AppHomeNav());

        // 楼层数据
        $this->assign('goods_floor_list', GoodsService::HomeFloorList());

        // 文章查询条件
        $params = [
            'where' => [],
            'field' => 'a.id,a.title,a.title_color,ac.name AS category_name, ac.alias_name',
            'm' => 0,
            'n' => 2,
        ];

        $gonggao_article_where = ['a.is_enable'=>1, 'a.is_home_recommended'=>1, 'article_category_id' => 10];
        $params['where'] = $gonggao_article_where;
        $gg_article_list = ArticleService::ArticleList($params);

        $hd_article_where = ['a.is_enable'=>1, 'a.is_home_recommended'=>1, 'article_category_id' => 25];
        $params['where'] = $hd_article_where;
        $hd_article_list = ArticleService::ArticleList($params);

        $this->assign('gg_article_list', $gg_article_list['data']);
        $this->assign('hd_article_list', $hd_article_list['data']);

        //规则帮助
        $help_params = [
            'where' => [],
            'field' => 'a.id,a.title,a.title_color,ac.name AS category_name, a.seo_title',
            'm' => 0,
            'n' => 2,
        ];
        $gz_article_where = ['a.is_enable'=>1, 'a.is_home_recommended'=>1, 'category_id' => 13];
        $help_params['where'] = $gz_article_where;
        $gz_article_list = HelpService::ArticleList($help_params);
        $xs_article_where = ['a.is_enable'=>1, 'a.is_home_recommended'=>1, 'category_id' => 14];
        $help_params['where'] = $xs_article_where;
        $xs_article_list = HelpService::ArticleList($help_params);

        $this->assign('gz_article_list', $gz_article_list['data']);
        $this->assign('xs_article_list', $xs_article_list['data']);

        // 用户订单状态
        $user_order_status = OrderService::OrderStatusStepTotal(['user_type'=>'user', 'user'=>$this->user, 'is_comments'=>1]);
        $this->assign('user_order_status', $user_order_status['data']);

        // 合作客户
        $custimer_params = [
            'where' => ['is_enable'=>1],
            'field' => '*',
            'm' => 0,
            'n' => 9,
        ];
        $customer_list = CustomerService::CustomerList($custimer_params);
        $this->assign('customer_list', $customer_list['data']);

        // 钩子
        $this->PluginsHook();
        
        return $this->fetch();
    }

    /**
     * 钩子处理
     * @author   Devil
     * @blog    http://gong.gg/
     * @version 1.0.0
     * @date    2019-04-22
     * @desc    description
     * @param   [array]           $params [输入参数]
     */
    private function PluginsHook($params = [])
    {
        // 楼层数据顶部钩子
        $hook_name = 'plugins_view_home_floor_top';
        $this->assign($hook_name.'_data', Hook::listen($hook_name,
            [
                'hook_name'     => $hook_name,
                'is_backend'    => false,
                'user'          => $this->user,
            ]));

        // 楼层数据底部钩子
        $hook_name = 'plugins_view_home_floor_bottom';
        $this->assign($hook_name.'_data', Hook::listen($hook_name,
            [
                'hook_name'     => $hook_name,
                'is_backend'    => false,
                'user'          => $this->user,
            ]));

        // 轮播混合数据底部钩子
        $hook_name = 'plugins_view_home_banner_mixed_bottom';
        $this->assign($hook_name.'_data', Hook::listen($hook_name,
            [
                'hook_name'     => $hook_name,
                'is_backend'    => false,
                'user'          => $this->user,
            ]));
    }
}
?>