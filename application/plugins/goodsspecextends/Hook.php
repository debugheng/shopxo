<?php
// +----------------------------------------------------------------------
// | ShopXO 国内领先企业级B2C免费开源电商系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2019 http://shopxo.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Devil
// +----------------------------------------------------------------------
namespace app\plugins\goodsspecextends;

use think\Controller;
use app\plugins\homemiddleadv\service\Service;
use app\service\PluginsService;

/**
 * 商品扩展数据插件 - 钩子入口
 */
class Hook extends Controller
{
    /**
     * 应用响应入口
     * @param    [array]                    $params [输入参数]
     */
    public function run($params = [])
    {
        if(!empty($params['hook_name']))
        {
            switch($params['hook_name'])
            {
                // 楼层数据上面
                case 'plugins_service_goods_spec_extends_handle' :
                    $ret = '';//$this->AdminGoodsSpecExtends($params)->getContent();
                    break;
                default :
                    $ret = '';
            }
            return $ret;
        }
    }

    /**
     * 商品扩展数据
     * @param    [array]          $params [输入参数]
     */
    public function AdminGoodsSpecExtends($params = [])
    {
        // 获取应用数据
        $ret = PluginsService::PluginsData('goodsspecextends');

        if($ret['code'] == 0 && !empty($ret['data']))
        {
            $this->assign('data_list', $ret['data']);
            return $this->fetch('../../../plugins/view/goodsspecextends/admin/admin/content');
        }
        return '';
    }
}
?>