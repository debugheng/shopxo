<?php
// +----------------------------------------------------------------------
// | ShopXO 国内领先企业级B2C免费开源电商系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2019 http://shopxo.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Devil
// +----------------------------------------------------------------------
namespace app\plugins\goodsspecextends\admin;

use think\Controller;
use app\service\PluginsService;

/**
 * 商品扩展数据插件 - 管理
 * @author   LeeHeng
 * @blog     http://codelee.ga/
 * @version  0.0.1
 * @datetime 2020-04-29T21:51:08+0800
 */
class Admin extends Controller
{
    public function index(){
        $ret = PluginsService::PluginsData('goodsspecextends', null, false);
        if($ret['code'] == 0)
        {
            return $this->fetch('../../../plugins/view/goodsspecextends/admin/index');
        } else {
            return $ret['msg'];
        }
    }
}
?>