<?php
// +----------------------------------------------------------------------
// | ShopXO 国内领先企业级B2C免费开源电商系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2019 http://shopxo.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Devil
// +----------------------------------------------------------------------
namespace app\plugins\homefloorlist\admin;

use think\Controller;
use app\plugins\homefloorlist\service\Service;
use app\service\PluginsService;

/**
 * 首页楼层插件 - 管理
 * @author   LeeHeng
 * @blog     http://codelee.ga/
 * @version  0.0.1
 * @datetime 2020-04-29T21:51:08+0800
 */
class Admin extends Controller
{
    /**
     * 首页
     * @author   LeeHeng
     * @blog     http://codelee.ga/
     * @version  0.0.1
     * @datetime 2020-04-29T21:51:08+0800
     * @param    [array]          $params [输入参数]
     */
    public function index($params = [])
    {
        $ret = PluginsService::PluginsData('homefloorlist', null, false);
        if($ret['code'] == 0)
        {
            // 数据列表
            $list = Service::DataList();
            $this->assign('data_list', $list['data']);

            $this->assign('data', $ret['data']);
            return $this->fetch('../../../plugins/view/homefloorlist/admin/admin/index');
        } else {
            return $ret['msg'];
        }
    }

    /**
     * 编辑页面
     * @author   LeeHeng
     * @blog     http://codelee.ga/
     * @version  0.0.1
     * @datetime 2020-04-29T21:51:08+0800
     * @param    [array]          $params [输入参数]
     */
    public function saveinfo($params = [])
    {
        // 数据
        $data = [];
        if(!empty($params['id']))
        {
            $data_params = [
                'get_id'    => $params['id'],
            ];
            $ret = Service::DataList($data_params);
            $data = empty($ret['data']) ? [] : $ret['data'];
        }
        $this->assign('data', $data);

        return $this->fetch('../../../plugins/view/homefloorlist/admin/admin/saveinfo');
    }

    /**
     * 数据保存
     * @author   Devil
     * @blog     http://gong.gg/
     * @version  1.0.0
     * @datetime 2019-02-07T08:21:54+0800
     * @param    [array]          $params [输入参数]
     */
    public function save($params = [])
    {
        // 是否ajax请求
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始处理
        return Service::DataSave($params);
    }

    public function floordata($params = []){
        // 数据
        $data = [];
        if(!empty($params['data_id']))
        {
            $data_params = [
                'data_id'    => $params['data_id'],
                'get_id'     => isset($params['id']) ? $params['id'] : 0,
            ];
            $ret = Service::FloorDataList($data_params);
            $data = empty($ret['data']) ? [] : $ret['data'];
        }
        $this->assign('data', $data);

        return $this->fetch('../../../plugins/view/homefloorlist/admin/admin/datalist');
    }

    public function savefloorinfo($params = [])
    {
        // 数据
        $data = [];
        if(!empty($params['id']))
        {
            $data_params = [
                'data_id'    => $params['data_id'],
                'get_id'     => isset($params['id']) ? $params['id'] : 0,
            ];
            $ret = Service::FloorDataList($data_params);
            $data = empty($ret['data']) ? [] : $ret['data'];
        }

        $this->assign('params', $params);
        $this->assign('data', $data);

        return $this->fetch('../../../plugins/view/homefloorlist/admin/admin/savefloorinfo');
    }

    public function floordatasave($params = []){
        // 是否ajax请求
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始处理
        return Service::FloorDataSave($params);
    }

    /**
     * 数据列表删除
     * @author   Devil
     * @blog     http://gong.gg/
     * @version  0.0.1
     * @datetime 2016-12-15T11:03:30+0800
     * @param    [array]          $params [输入参数]
     */
    public function floordatadelete($params = [])
    {
        // 是否ajax请求
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始处理
        return Service::FloorDataDelete($params);
    }
}
?>