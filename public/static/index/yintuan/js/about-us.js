/**
 * Created by hyt007 on 2017/8/2.
 */
$(function () {
  $(".banner-img").css(
      {
       "top":"0"
      }
  );
  $(".banner-info").css(
      {
       "bottom":"100px"
      }
  );
//  联系我们  次级导航切换

  $(".second-nav").find("li").on("click",function () {
          var navList= $(".second-nav").find("li"),
              moveEle=$(".move-bg"),
              navWidth=navList.width(),
              $index=$(this).index(),
              itemList=$(".contact-us-item");
          navList.removeClass("second-nav-sel");
          moveEle.animate({"left":$index*navWidth},300);
          $(this).addClass("second-nav-sel");
          itemList.hide().eq($index).show();
  });
//网站建议按钮
  $("#web-suggest").on("click",function () {
    if(window.localStorage){
      localStorage.setItem("webSuggest","true");
    }
  })

//加入我们 岗位切换
 $(".join-us-nav h3").on("click",function () {
    if($(this).hasClass("join-us-sel")){
      return ;
    }
    else{
      $(".join-us-nav h3").removeClass("join-us-sel");
      $(this).addClass("join-us-sel");
      var $index=$(this).index(".join-us-nav h3");
      if( $index=="1"){
        $(".join-us-wrap").eq(0).animate({left:"-1170px"},function(){
          $(this).hide();
          $(".join-us-wrap").eq(1).show().animate({left:"0"});
        });
        
      }
      else{
        $(".join-us-wrap").eq(1).animate({left:"1170px"},function(){
          $(this).hide();
          $(".join-us-wrap").eq(0).show().animate({left:"0"});
        });
      
      }
     
    }
  })
})