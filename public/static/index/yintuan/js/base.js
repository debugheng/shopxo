﻿/*===========================
 *作者：动力启航(谢凯)
 *网址：http://www.it134.cn
 *转发请保留作者信息，谢谢
===========================*/

//=====================全局函数========================
//Tab控制函数
function tabs(tabId, tabNum){
	//设置点击后的切换样式
	$(tabId + " .tab li").removeClass("curr");
	$(tabId + " .tab li").eq(tabNum).addClass("curr");
	//根据参数决定显示内容
	$(tabId + " .tabcon").hide();
	$(tabId + " .tabcon").eq(tabNum).show();
}
//=====================全局函数========================

//==================图片详细页函数=====================
//鼠标经过预览图片函数
function preview(img){
	$("#preview .jqzoom img").attr("src",$(img).attr("src"));
	$("#preview .jqzoom img").attr("jqimg",$(img).attr("bimg"));
	$(img).parent().siblings().removeClass("order_selected_big");
	$(img).parent().addClass("order_selected_big");
}

//图片放大镜效果
$(function(){
	$(".jqzoom").jqueryzoom({xzoom:380,yzoom:410});
});

//图片预览小图移动效果,页面加载时触发
$(function(){
	var tempLength = 0; //临时变量,当前移动的长度
	var viewNum = 4; //设置每次显示图片的个数量
	var moveNum = 1; //每次移动的数量
	var moveTime = 300; //移动速度,毫秒
	var scrollDiv = $(".spec-scroll .items ul"); //进行移动动画的容器
	var scrollItems = $(".spec-scroll .items ul li"); //移动容器里的集合
	var moveLength = scrollItems.eq(0).width() * moveNum; //计算每次移动的长度
	var countLength = (scrollItems.length - viewNum) * scrollItems.eq(0).width(); //计算总长度,总个数*单个长度
	  
	//下一张
	$(".spec-scroll .next").bind("click",function(){
		if(tempLength < countLength){
			if((countLength - tempLength) > moveLength){
				scrollDiv.animate({left:"-=" + moveLength + "px"}, moveTime);
				tempLength += moveLength;
			}else{
				scrollDiv.animate({left:"-=" + (countLength - tempLength) + "px"}, moveTime);
				tempLength += (countLength - tempLength);
			}
		}
	});
	//上一张
	$(".spec-scroll .prev").bind("click",function(){
		if(tempLength > 0){
			if(tempLength > moveLength){
				scrollDiv.animate({left: "+=" + moveLength + "px"}, moveTime);
				tempLength -= moveLength;
			}else{
				scrollDiv.animate({left: "+=" + tempLength + "px"}, moveTime);
				tempLength = 0;
			}
		}
	});
});
//==================图片详细页函数=====================


//专版
$(function(){
	//下拉选择
	$(".order_slideDown_click").click(function(event){
		$(".order_slideDown_item").not($(this).siblings("ul")).hide();
		$(this).siblings("ul").toggle();
		event.stopPropagation();
	})
	$(document).click(function(){
		$(".order_slideDown_item").hide();
	})
	//$(".order_slideDown_item li").click(function(){
	//	$(this).parent().siblings("h3").html($(this).text()+"<i></i>");
	//})
	$(".order_slideDown_item li").live('click',function(){
		$(this).parent().siblings("h3").html($(this).text()+"<i></i>");
		var dt = $(this).attr('data');
		var dtp = $(this).attr('data-type');
		$('#pw'+dtp+'').val(dt);
	})
	
	//选择产品参数
	$(".order_item_item li").click(function(){
        if($(this).find('a').text() == '粘单拼手袋'){
            
            	$('.order_num5_13').hide();
            	$('#alike').val(0);
                $(this).siblings().find(".order_input_bolck").hide();
                $(this).find(".order_input_bolck").css("display","inline-block");
                if($(this).hasClass("order_selected")){

                    if ($('#alike_li').hasClass('order_selected')) {
                        $('#alike_li').removeClass("order_selected").siblings('li').addClass('order_selected');
                    }
                    return;
                }else{
                    $(this).siblings().removeClass("order_selected");
                    $(this).addClass("order_selected");

                    if ($('#alike_li').hasClass('order_selected')) {
                        $('#alike_li').removeClass("order_selected").siblings('li').addClass('order_selected');
                    }
                }
          
        }else if($(this).find('a').text() == '粘双拼手袋'){

            	$('.order_num5_13').show();
                $(this).siblings().find(".order_input_bolck").hide();
                $(this).find(".order_input_bolck").css("display","inline-block");
                if($(this).hasClass("order_selected")){
                	
                    return;
                }else{
                    $(this).siblings().removeClass("order_selected");
                    $(this).addClass("order_selected");
                    
                }

           
        }else{
            $(this).siblings().find(".order_input_bolck").hide();
            $(this).find(".order_input_bolck").css("display","inline-block");
	        if ($(this).hasClass('disable')) {

	            return false;
	        }
            if($(this).hasClass("order_selected")){
                return;
            }else{
                $(this).siblings().removeClass("order_selected");
                $(this).addClass("order_selected");
            }
        }
	})

	
	//产品介绍、注意事项切换
	$(".order_tab li").click(function(){
		var i=$(this).index();
		$(this).siblings().removeClass("order_tab_selected");
		$(this).addClass("order_tab_selected");
		$(".order_content").hide();
		$(".order_content").eq(i).show();
	})
	
	//加减输入框数字
	$(".order_sub").click(function(){
		var v=parseInt($(this).siblings("input").val());
		v-=4;
		if(v<=0){
			return;
		}
		$(this).siblings("input").val(v);
	})
	$(".order_add").click(function(){
		var v=parseInt($(this).siblings("input").val());
		v+=4;
		if(v>10000){
			return;
		}
		$(this).siblings("input").val(v);
	})
	//自动报价弹窗
	/*
	$(".order_num8_b .order_num8_b_a").click(function(){
		$(".bj_Order").show();
	})
	$(".bj_Order_close").click(function(){
		$(".bj_Order").hide();
	})*/

	$(".order_num8_b .order_num8_b_a").click(function(){
		$(".order_right div.orderHb_s5,.order_right div.orderHb_s4").show();
		$(".orderHb_s1_p2").show();
	})


	//关闭qq列表
	$(".orderHb_qq_open").click(function(){
		$(".orderHb_qq_box").show();
	})
	$(".orderHb_qq_close").click(function(){
		$(".orderHb_qq_box").hide();
	})

})

//合版
$(function(){
	
	$(".li-hover").click(function(){
		var attrType = $(this).attr('attrType');
		if($(this).attr('data') == 'custom' && attrType == '数量'){
			$('#inputCustom').val(1);
			$('#costModel').val(1);
		}
		if(attrType == '数量' && $(this).attr('data') != 'custom'){
			$('#inputCustom').val(0);
			$('#costModel').val(1);
		}

		if ($(this).hasClass('disable')) {

			return;
		}

		if($(this).hasClass("order_ulist_select")){
			return;
		}else{
			$(this).siblings().removeClass("order_ulist_select");
			$(this).addClass("order_ulist_select");
		}
	})
	/*
	$(".order_left_i").click(function(){
		var v=parseInt($(this).siblings("input").val());
		v-=1;
		if(v<=0){
			return;
		}
		$(this).siblings("input").val(v);
	})
	$(".order_right_i").click(function(){
		var v=parseInt($(this).siblings("input").val());
		v+=1;
		if(v>10000){
			return;
		}
		$(this).siblings("input").val(v);
	})*/

	//展示更多
	/*
	$(".order_uitem1 li").each(function(index) {
		//alert($(this).position().top);
        if(index>5){
			$(this).hide();
		}
		if($(this).position().top>0){
			$(this).hide();
		}
    });
	$(".orderHb_s2_more1").click(function(){
		$(".order_uitem1 li").each(function(index) {
			if(index>5){
				$(this).slideToggle();
			}
		});
	})

	$(".order_uitem2 li").each(function(index) {
        if(index>6){
			$(this).hide();
		}
    });
	$(".orderHb_s2_more2").click(function(){
		$(".order_uitem2 li").each(function(index) {
			if(index>6){
				$(this).slideToggle();
			}
		});
	})*/


	$(".order_uitem li").each(function(index) {
		var topH=$(this).offset().top-$(this).parent().offset().top;
		if(topH>10){
			$(this).hide();
			$(this).parent().siblings("a").show();
		}
	});

	$(".orderHb_s2_more").toggle(function(){
		var more_i1=0;
		var more_i2=$(this).siblings("ul").find("li").length;
		$(this).siblings("ul").find("li").each(function(index) {
			if($(this).css("display")=="none"){
				more_i1++;
			}
		})
		var more_i=more_i2-more_i1-1;
		$(this).siblings("ul").find("li:gt("+more_i+")").slideDown();
	},function(){
		$(this).siblings("ul").find("li").each(function(index) {
			if($(this).position().top>0){
				$(this).slideUp()
			}
		});
	})

	$(".order_hide").hide();
	$(".orderHb_s2_more2").click(function(){
		$(".order_hide").slideToggle(500);
	})
	//显示内页二
	$(".order_num3_more").click(function(){
		$(".order_num4").slideToggle(500);
	})

	//tab
	$(".orderHb_tab li").click(function(){
		var i=$(this).index();
		$(this).siblings().removeClass("orderHb_tab_selected");
		$(this).addClass("orderHb_tab_selected");
		$(".orderHb_content").hide();
		$(".orderHb_content").eq(i).show();
        $('#orderIfram').iframeAutoHeight();
        $('#commentIfram').iframeAutoHeight();
	})
	
	//烫金
	$(".order_selected").find(".order_input_bolck").css("display","inline-block");
	//ul 最后一个li的右边距设置为0
	$(".order_item_item").each(function(index) {
		var lastI=$(this).find("li").length-1;
        $(this).find("li:eq("+lastI+")").css("margin-right","0");
    });
	$(".order_uitem").each(function(index) {
		var lastI=$(this).find("li").length-1;
        $(this).find("li:eq("+lastI+")").css("margin-right","0");
    });
})