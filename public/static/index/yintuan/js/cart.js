// JavaScript Document
$(function(){
    changebg()
    $(".cart_list_check_all").click(function(){
        if(this.checked){
            $(".cart_list :checkbox").attr("checked", true);
            $(".cart_delete_checkall").attr("checked", true);
            updateCount();
        }else{
            $(".cart_list :checkbox").attr("checked", false);
            $(".cart_delete_checkall").attr("checked", false);
            updateCount();
        }
        changebg();
    })
    $(".cart_delete_checkall").click(function(){
        if(this.checked){
            $(".cart_list  :checkbox").attr("checked", true);
            $(".cart_list_check_all").attr("checked", true);
            updateCount();
        }else{
            $(".cart_list  :checkbox").attr("checked", false);
            $(".cart_list_check_all").attr("checked", false);
            updateCount();
        }
        changebg();
    })
    $(".cart_list  input:checkbox").click(function(){
        var checkboxI=$(".cart_list  input:checkbox").length;
        if($(this).attr("checked")){
            $(this).parent().parent().css({"background-color":"#fcfcfc"});
        }
        else{
            $(this).parent().parent().css({"background-color":"#fff"});
        }
        $(".cart_list  input:checkbox").each(function(index, element) {
            //alert($(this).attr("checked"))
            if(!$(this).attr("checked")){
                checkboxI--;
                //alert(checkboxI)
                $(".cart_list_check_all,.cart_delete_checkall").attr("checked", false);
            }
            else{
                if(checkboxI==$(".cart_list  input:checkbox").length){
                    $(".cart_list_check_all,.cart_delete_checkall").attr("checked", true);
                }
            }
        });
    })
})
function changebg(){
    $(".cart_list input:checkbox").each(function(index, element) {
        //alert($(this).attr("checked"))
        if($(this).attr("checked")){
            $(this).parent().parent().css({"background-color":"#fcfcfc"});
        }
        else{
            $(this).parent().parent().css({"background-color":"#fff"});
        }
    });
}