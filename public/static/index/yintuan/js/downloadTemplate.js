/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-10-11 14:21:58
 * @LastEditTime: 2019-10-11 14:37:03
 * @LastEditors: Please set LastEditors
 */
/**
 * Created by 张郑馥 on 2019/10/11.
 */


$(function () {   
  //  **** 免费刀版导航切换函数 ****
    var freeServiceNav=function(navClass,cntClass){
      $(navClass).on("click",function () {
           var $index=$(this).index();
           $(cntClass).hide();
           $(cntClass).eq($index).show();
           $(navClass).removeClass("nav-sel").find(".nav-sel-mark").remove();
           $(this).addClass("nav-sel").append('<i class="nav-sel-mark"></i>');
      })
    }
    freeServiceNav(".free-template-box li",".free-service-item");
  })
  