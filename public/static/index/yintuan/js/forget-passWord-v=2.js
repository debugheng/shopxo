;
$(function () {

    //全局获取焦点隐藏提示信息
    $('body').on('focus', 'input', function () {
        $('.tip').hide();
    })

    //确认手机号是否已注册
    $('body').on('click', '.next-btn', function () {
        var phoneValue = $('#phone-num').val(),
            imgCode = $('#code-img').val();

        if (phoneValue == '') {
            $('#phone-num-tip').show().find('.tip-text').text('请输入账号')
        } else if (imgCode == '') {
            $('#code-img-tip').show().find('.tip-text').text('请输入图形验证码')
        } else if (phoneValue.length < 11 && phoneValue.length > 0) {
            $('#phone-num-tip').show().find('.tip-text').text('请输入11位电话号码')
        } else {
            //ajax 异步请求 验证手机和验证码
            $.ajax({
                type: "POST",
                // url: "http://mall.local/www/findPwd/checkUsername",
                url: "https://www.intuan.com/www/findPwd/checkUsername",
                data: {
                    "code": imgCode,
                    "username": phoneValue
                },
                success: function (res) { //返回数据根据结果进行相应的处理 
                    //这里处理返回后的事情
                    if(res['success'] == 0){

                        layer.msg(res['msg']);
                    }else{
                        // checkPhone
                        // window.location.href = "http://mall.local/www/findPwd/checkPhone";
                        window.location.href = "https://www.intuan.com/www/findPwd/checkPhone";
                        // layer.alert(res['msg']);
                    }
                }
            })
        }

    })

    //发送短信
    $('body').on('click', '.send-msg', function () {
        $('.verify-effect-wrap').hide();
        $('.verify-send-wrap').show();
        $('.intuan-btn').removeClass('send-msg').addClass('affirm-btn').text("确定");
        fun_timedown(120);
    })

    //确定手机验证码
    $('body').on('click', '.affirm-btn', function () {
        var codeValue = $('#code-phone').val();
        if (codeValue == '') {
            $('#code-tip').show().find('.tip-text').text("请填写手机验证码");
        } else {
            // ajax异步请求验证手机验证码
            $.ajax({
                type: "POST",
                // url: "http://mall.local/www/findPwd/checkCode",
                url: "https://www.intuan.com/www/findPwd/checkCode",
                data: {
                    "code": codeValue
                },
                success: function (res) { //返回数据根据结果进行相应的处理 
                    res = eval('(' + res + ')');
                    if (res['success'] == 1) {
                        // window.location.href = "http://mall.local/www/findPwd/resetPwd";
                        window.location.href = "https://www.intuan.com/www/findPwd/resetPwd";
                    } else {
                        $('#code-tip').show().find('.tip-text').text("手机验证码错误或已过期");
                    }
                }
            })
        }
    })

    //重置密码
    $('body').on('click', '.submit-btn', function () {
        var resizeValue = $('#resize-psw').val(),
            affirmValue = $('#affirm-psw').val();
        // consoloe.log(resizeValue);
        if (resizeValue == '') {
            $('#resize-psw-tip').show().find('.tip-text').text('请填写密码')
        } else if (affirmValue == '') {
            $('#affirm-psw-tip').show().find('.tip-text').text('请确认密码')
        } else if (resizeValue.length > 0 && resizeValue.length < 8) {
            $('#resize-psw-tip').show().find('.tip-text').text('请填写8位以上的安全密码')
        } else if (affirmValue !== resizeValue) {
            $('#affirm-psw-tip').show().find('.tip-text').text('两次密码不一致')
        } else {
            //ajax异步请求验证手机验证码
            // $.ajax({
            //     type: "POST",
            //     url: "",
            //     data: {
            //         "resizeValue": resizeValue
            //     },
            //     success: function (res) { //返回数据根据结果进行相应的处理 
            //     }
            // })

            $.ajax({

                // url: "http://mall.local/www/findPwd/newPwd",
                url: "https://www.intuan.com/www/findPwd/newPwd",
                data: {password:resizeValue, repassword:affirmValue},
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                 if(data['success'] == 1){

                    layer.msg(data['msg']);
                    setTimeout(function(){
                        // window.location.href = "http://mall.local/www/login/index";
                        window.location.href = "https://www.intuan.com/www/login/index";
                    }, 2000);

                 }else if(data['success'] == 2){

                    layer.msg(data['msg']);
                    setTimeout(function(){
                        // window.location.href = "http://mall.local/www/findPwd/index";
                        window.location.href = "https://www.intuan.com/www/findPwd/index";
                    }, 2000);
                 }else{
                    
                    layer.msg(data['msg']);
                 }
                },
         });

        }
    })

})


//倒计时
function fun_timedown(time) {
    $(".code-img").removeClass("send-msg");
    $(".time").html("120秒");
    if (time == 'undefined')
        time = 120;
    $(".time").html(time + "秒");
    time = time - 1;
    if (time >= 0) {
        setTimeout("fun_timedown(" + time + ")", 1000);
    } else {
        $(".time").html("重新发送");
        $(".code-img").addClass("send-msg");
    }
}