// JavaScript Document
$(function(){
	var nums=$(".lb_item li").size();
	//alert(nums);
	for(var j=0;j<nums;j++){
		//var li="<li>"+j+"</li>";
		var li="<li></li>";
		$(".lb_index").append(li);
	}
	
	$(".lb_list").eq(0).addClass("lb_show");
	$(".lb_index li").eq(0).addClass("lb_on");
	
	
	var i=0;
	$(".lb_index li").mouseover(function(){
		$(this).addClass("lb_on").siblings().removeClass("lb_on");
		i=$(this).index();
		$(".lb_list").removeClass("lb_show");
		$(".lb_list").eq(i).stop().fadeIn(300).siblings().stop().fadeOut(300);
	})
	
	function lb_next(){
		//alert(i)
		i++;
		if(i>nums-1){
			i=0;
		}
		$(".lb_index li").eq(i).addClass("lb_on").siblings().removeClass("lb_on");
		$(".lb_list").removeClass("lb_show");
		$(".lb_list").eq(i).stop().fadeIn(300).siblings().stop().fadeOut(300);
	}
	
	var t=setInterval(lb_next,3000);
	
	function lb_prev(){
		i--;
		if(i<0){
			i=nums-1;
		}
		//alert(i)
		$(".lb_index li").eq(i).addClass("lb_on").siblings().removeClass("lb_on");
		$(".lb_list").removeClass("lb_show");
		$(".lb_list").eq(i).fadeIn(300).siblings().fadeOut(300);
	}
	$(".lb_right").click(function(){
		lb_next();
	});
	$(".lb_left").click(function(){
		lb_prev();
	});
	
	$(".lb_block_box").hover(function(){
		clearInterval(t);
		$(".lb_btn").show();
	},function(){
		t=setInterval(lb_next,3000);
		$(".lb_btn").hide();
	})
})

$(function(){
	//导航栏下滑线
	var index1=1;
	var i_left=parseInt($(".head_nav_item_last").css("left"));
	
	$(".head_nav_item li").not(".head_nav_item_last").hover(function(){
		$(this).siblings().stop();
		var mindex=$(this).index();
		if(mindex==0){
			$(".head_nav_item_last").animate({"left":"24px"},300);
		}
		else if(mindex==1){
			$(".head_nav_item_last").animate({"left":"119px"},300);
		}
		else{
			$(".head_nav_item_last").animate({"left":(mindex-index1)*112+119+"px"},300);
		}
	},function(){
		$(this).siblings().stop();
		if(isNaN(i_left)){
			i_left=24;
		}
		$(".head_nav_item_last").animate({"left":i_left+"px"},300);	
	})
	
	
	//菜单栏
	$(".nav_dt_index").on("click",$(".nav_dt_index"),function(){
		$(".nav_dd").toggle();
		$(".nav_dt i").toggleClass("i_click");		
	})
	$(".head_nav_l_o").mouseenter(function(){
		$(".nav_dd").show();
		$(".nav_dt i").addClass("i_click");		
	})
	$(".head_nav_l_o").mouseleave(function(){
		$(".nav_dd").hide();
		$(".nav_dt i").removeClass("i_click");		
	})
	//导航
	$(".dd_show .dd_show_list").hover(function(){
		$(this).addClass("dd_show_hover");
		$(this).find(".dd_hide_list").addClass("dd_hide_show");
	},function(){
		$(this).removeClass("dd_show_hover");
		$(this).find(".dd_hide_list").removeClass("dd_hide_show");
	})
	/*$(".dd_show .dd_show_list").mouseover(function(){
		var nav_index=$(this).index();
		$(this).addClass("dd_show_hover")
		$(".dd_hide").show();
		$(".dd_hide .dd_hide_list").eq(nav_index).addClass("dd_hide_show");
	})*/
	
	//地址
	$(".sec_item").click(function(){
		$(".sec_item").removeClass("sec_item_active");
		$(this).addClass("sec_item_active");
		$(".header_top_add_span").text($(this).text());
	})
	
	//右侧悬浮框
	$(".toolbar-block").hover(function(){
		$(this).addClass("toolbar-hover");
		$(this).siblings().find("em").stop(false, true);
		$(this).find("em").animate({left:"-60px"});
	},
	function(){
		$(this).removeClass("toolbar-hover");
		$(this).siblings().find("em").stop(false, true);
		$(this).find("em").animate({left:"35px"});
	}
	)
	//返回顶部
	$(".toolbar-top").click(function(){
		$("html,body").animate({scrollTop:"0"})
	})
	
	ScrollText($('#scrollText'),48,500,$("#scrollText").text(),'left',1,20);//滚动字幕
})
var ScrollTime;
function ScrollAutoPlay(contID,scrolldir,showwidth,textwidth,steper){
 var PosInit,currPos;
 with($('#'+contID)){
  currPos = parseInt(css('margin-left'));
  if(scrolldir=='left'){
   if(currPos<0 && Math.abs(currPos)>textwidth){
    css('margin-left',showwidth);
   }
   else{
    css('margin-left',currPos-steper);
   }
  }
  else{
   if(currPos>showwidth){
    css('margin-left',(0-textwidth));
   }
   else{
    css('margin-left',currPos-steper);
   }
  }
 }
}

//--------------------------------------------左右滚动效果----------------------------------------------
/*
AppendToObj：  显示位置（目标对象）
ShowHeight：  显示高度
ShowWidth：  显示宽度
ShowText：   显示信息
ScrollDirection： 滚动方向（值：left、right）
Steper：   每次移动的间距（单位：px；数值越小，滚动越流畅，建议设置为1px）
Interval:   每次执行运动的时间间隔（单位：毫秒；数值越小，运动越快）
*/
function ScrollText(AppendToObj,ShowHeight,ShowWidth,ShowText,ScrollDirection,Steper,Interval){
 var TextWidth,PosInit,PosSteper;
 with(AppendToObj){
  html('');
  css('overflow','hidden');
  css('height',ShowHeight+'px');
  css('line-height',ShowHeight+'px');
  css('width',ShowWidth);
 }
 if (ScrollDirection=='left'){
  PosInit = ShowWidth;
  PosSteper = Steper;
 }
 else{
  PosSteper = 0 - Steper;
 }
 if(Steper<1 || Steper>ShowWidth){Steper = 1}//每次移动间距超出限制(单位:px)
 if(Interval<1){Interval = 10}//每次移动的时间间隔（单位：毫秒）
 var Container = $('<div></div>');
 var ContainerID = 'ContainerTemp';
 var i = 0;
 while($('#'+ContainerID).length>0){
  ContainerID = ContainerID + '_' + i;
  i++;
 }
 with(Container){
   attr('id',ContainerID);
   css('float','left');
   css('cursor','pointer');
   appendTo(AppendToObj);
   html(ShowText);
   TextWidth = width();
   if(isNaN(PosInit)){PosInit = 0 - TextWidth;}
   css('margin-left',PosInit);
   mouseover(function(){
    clearInterval(ScrollTime);
   });
   mouseout(function(){
    ScrollTime = setInterval("ScrollAutoPlay('"+ContainerID+"','"+ScrollDirection+"',"+ShowWidth+','+TextWidth+","+PosSteper+")",Interval);
   });
 }
 ScrollTime = setInterval("ScrollAutoPlay('"+ContainerID+"','"+ScrollDirection+"',"+ShowWidth+','+TextWidth+","+PosSteper+")",Interval);
}