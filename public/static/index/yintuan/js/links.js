/**
 * Created by zzf on 2017/8/7.
 */

$(function () {
  $(".links-banner-info h1").css({
    "top": "32px"
  });
  $(".links-banner-info .web-info-box").css({
    "bottom": "26px"
  });
  $(".copy-code-btn").on("click", function () {
    $(this).prev().select(); // 选择对象
    document.execCommand("Copy"); // 执行浏览器复制命令
    alert("已复制好，可贴粘");
  });
})
