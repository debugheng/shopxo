// JavaScript Document
$(function(){
	//菜单栏
	$(".nav_dt_index").on("click",function(){
		$(".nav_dd").toggle();
		$(".nav_dt").toggleClass("cur");		
	})
	$(".head_nav_l_o").mouseenter(function(){
		$(".nav_dd").show();
		$(".nav_dt i").addClass("i_click");		
	})
	$(".head_nav_l_o").mouseleave(function(){
		$(".nav_dd").hide();
		$(".nav_dt i").removeClass("i_click");		
	})
	
	//地址选择
	$(".sec_item").click(function(){
		$(".sec_item").removeClass("sec_item_active");
		$(this).addClass("sec_item_active");
		$(".header_top_add_span").text($(this).text());
	})
	
	//导航
	$(".dd_show .dd_show_list").hover(function(){
		$(".nav_dd_box").css("width","925px");
		$(this).addClass("dd_show_hover");
		$(this).find(".dd_hide_list").addClass("dd_hide_show");
	},function(){
		$(".nav_dd_box").css("width","171px");
		$(this).removeClass("dd_show_hover");
		$(this).find(".dd_hide_list").removeClass("dd_hide_show");
	})
	//导航栏下滑线
	// var index1=1;
	/* 初始化头部主导航 */
	var initNav=function () {
    var $navSelectWidth=$(".head_nav_select").width()-12;

    if($(".head_nav_select").length>0){
      $(".head_nav_item_last").show()
      var $navSelectLeft=$(".head_nav_select").position().left;
      $(".head_nav_item_last").css({"width":$navSelectWidth+"px","left":$navSelectLeft+"px"});
		}
		else{
      $(".head_nav_item_last").hide();
    }
  }
	initNav();
	// var i_left=parseInt($(".head_nav_item_last").css("left"));
	$(".head_nav_item li").not(".head_nav_item_last").hover(function(){
    $(".head_nav_item_last").show()
		$(this).siblings().stop();
		// var mindex=$(this).index();
		var liWidth=$(this).width()-12;
		var leftWidth=$(this).position().left;
		$(".head_nav_item_last").css("width",liWidth+"px").animate({"left":leftWidth+"px"},300);
		// if(mindex==0){
		// 	$(".head_nav_item_last").animate({"left":"54px"},300);
		// }
		// else if(mindex==1){
		// 	$("li.head_nav_item_last").css("width","80px");
		// 	$(".head_nav_item_last").animate({"left":"150px"},300);
		// }
		// else if(mindex==5){
		// 	$("li.head_nav_item_last").css("width","93px");
		// 	$(".head_nav_item_last").animate({"left":"542px"},300);
		// }
		// else{
		// 	$("li.head_nav_item_last").css("width","80px");
		// 	$(".head_nav_item_last").animate({"left":(mindex)*97+54+"px"},300);
		// }
	},function(){
    var $navSelectWidth=$(".head_nav_select").width()-12;
    if ($(".head_nav_select").length>0){
      var $navSelectLeft=$(".head_nav_select").position().left;
      $(".head_nav_item_last").show()
      $(this).siblings().stop();
      $(".head_nav_item_last").css({"width":$navSelectWidth+"px"}).animate({"left":$navSelectLeft+"px"},300);
		}
		else{
      $(".head_nav_item_last").hide()
		}

		// if(isNaN(i_left)){
		// 	i_left=54;
		// }
		// $(".head_nav_item_last").animate({"left":i_left+"px"},300);
	})
	//底部切换
	$(".tabB a").hover(function(e) {
        $(this).addClass("tabB_cur");
		$(this).parent().siblings("li").find("a").removeClass("tabB_cur");
		$(this).parents(".tabB").siblings(".tabB_cont").children("div").hide();
		$(this).parents(".tabB").siblings(".tabB_cont").children("div").eq($(this).parent().index()).show();
    });
	$(".footer_timeTab li").hover(function(e) {
		var _index=$(this).index();
        $(this).addClass("cur").siblings("li").removeClass("cur");
		$(this).parent().siblings(".footer_time").find("p").hide().eq(_index).show();
    });
	
})

