; $(function () {

    var navPage = {

        // 初始化页面
        init: function () {
            this.toggleNavTap();
        },
        // **切换产品导航的标签
        toggleNavTap: function () {
            $('.nav-item').on('click', function () {
                var that = $(this);
                var kindValue=that.attr('data-kind');//获取当前点击标签的类别值
                //切换样式
                if (that.hasClass('nav-item-active')) {
                    return;
                }
                $('.nav-item-active').removeClass('nav-item-active');
                that.addClass('nav-item-active');
               
                //联动显示当前点击的标签
                if(!(kindValue==='all')){
                    $('.product-card-box').hide();
                    // $('.product-card-box[data-kind='+kindValue+']').show();

                    $('.kind_'+kindValue).show();
                }else{
                    $('.product-card-box').show();
                }
               
            })
        }
    }
    // 初始化
    navPage.init();

})