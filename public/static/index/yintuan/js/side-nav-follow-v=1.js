/**
 * Created by 张郑馥 on 2017/7/27.
 */

/*
 * **参数配置说明 **
 * "contentWidth": 960,  //网页有效的内容区域宽度
 * "navWidth": this.$element.width(),  //侧边导航的自身宽度
 * "viewWidth": $(window).width(),  //窗口可视区域宽度
 * "offTop": 100,  //距离顶部top的值
 * "positionStyle": "fixed",  //默认定位方式fixed
 * "offSide": 20, //当可视区域小于内容有效区域时距离边框的值
 * "navSide": "right", //导航放置位置，默认是右侧导航
 * "navEle":"li",//侧边导航列表项目的class或者html标签
 * "selClass":"right-nav-follow-sel",//导航选择的class
 * "hoverClass":"reght-nav-follow-hover",//导航hover时的class
 * "cntEle":".guide-item-box",//滚动内容的列表项class
 * "backTopOffset":"1200",//有返回顶部按钮
 * "backTop":".back-top",//有返回顶部按钮
 */

(function($, window, document, undefined) {
  //定义sideFollowNav的构造函数
  var sideFollowNav = function (ele, opt) {
    this.$element = ele;
    this.defaults = {
      "contentWidth": 960,  //网页有效的内容区域宽度
      "navWidth": this.$element.width(),  //侧边导航的自身宽度
      "viewWidth": $(window).width(),  //窗口可视区域宽度
      "offTop": 100,  //距离顶部top的值
      "positionStyle": "fixed",  //默认定位方式fixed
      "offSide": 20, //当可视区域小于内容有效区域时距离边框的值
      "navSide": "right" ,//导航放置位置，默认是右侧导航
      "backTopOffset":"1800"
    }
    this.option = $.extend({}, this.defaults, opt); //配置选项
  }
  //定义sideFollowNav的方法
  sideFollowNav.prototype = {
    //侧边导航定位
    sideNavLocation: function () {
      var thisObj = this;
      this.$element.css({
            "position": this.option.positionStyle,
            "top": this.option.offTop
          }
      );
       $(window).on("load resize ", function () {
        thisObj.option.viewWidth = $(window).width();
        if (thisObj.option.viewWidth > thisObj.option.contentWidth) {
          if (thisObj.option.navSide == "left") {
            thisObj.$element.css(
                {
                  "left": (thisObj.option.viewWidth - thisObj.option.contentWidth) / 2 - thisObj.option.navWidth + "px"
                }
            )
          }
          else {
            thisObj.$element.css(
                {
                  "right": (thisObj.option.viewWidth - thisObj.option.contentWidth) / 2 - thisObj.option.navWidth + "px"
                }
            )
          }
        }
        else {
          if (thisObj.option.navSide == "left") {
            thisObj.$element.css(
                {
                  "left": thisObj.option.offSide + "px"
                }
            )
          }
          else {
            thisObj.$element.css(
                {
                  "right": thisObj.option.offSide + "px"
                }
            )
          }
        }
      })
    },
    //侧边导航滚动和单击
    sideNavScroll: function () {
      var thisObj = this,
          cntList = $(thisObj.option.cntEle);
      var scrollCnt = function () {
        var scrollT = $(this).scrollTop();
        if (scrollT >= cntList.eq(0).offset().top - 100) {
          thisObj.$element.show();
        }
      
        if (scrollT < cntList.eq(0).offset().top - 100 || scrollT > cntList.eq(cntList.length - 1).offset().top+cntList.eq(cntList.length - 1).height()/2) {
          thisObj.$element.hide();
        }
        if(scrollT >= thisObj.option.backTopOffset){
          $(thisObj.option.backTop).show();
        }
        if(scrollT < thisObj.option.backTopOffset){
          $(thisObj.option.backTop).hide();
        }
        for (var i = 0; i < cntList.length; i++) {
          var cntTop = cntList.eq(i).offset().top - 100;
          if (scrollT >= cntTop) {
            thisObj.$element.find(thisObj.option.navEle).removeClass(thisObj.option.selClass);
            thisObj.$element.find(thisObj.option.navEle).eq(i).addClass(thisObj.option.selClass);
          }
          else {
            break;
          }
        }
      }
      //  绑定窗口滚动事件
      $(window).on("scroll", scrollCnt);
      // 侧边导航单击事件
        thisObj.$element.find(thisObj.option.navEle).not(this.option.backTop).on("click", function () {
        var i = $(this).index(),
            cntList = $(thisObj.option.cntEle);
        $(window).off("scroll", scrollCnt);//卸载scroll事件上scrollCnt函数（不会跳动）
        thisObj.$element.find(thisObj.option.navEle).removeClass(thisObj.option.selClass);
        thisObj.$element.find(thisObj.option.navEle).eq(i).addClass(thisObj.option.selClass);
        $("html,body").animate({"scrollTop": cntList.eq(i).offset().top - 100}, 300, function () {
          $(window).on("scroll", scrollCnt);//回调scroll事件上的scrollCnt函数
        });
      })
    },
    //侧边导航hover
    sideNavHover: function () {
      var thisObj = this,
          navEle = this.$element.find(this.option.navEle);
      navEle.hover(function () {
        $(this).addClass(thisObj.option.hoverClass);
      }, function () {
        $(this).removeClass(thisObj.option.hoverClass);
      })
    },
    //返回顶部
    navBackTop: function () {
      var thisNavObj = this.$element;
      if (this.option.backTop) {
        thisNavObj.find(this.option.backTop).on("click", function () {
          $("html,body").animate({"scrollTop": "0"}, 500);
        })
      }
    }
  };
  $.fn.sideNavSet = function (options) {
    var sideNavObj = new sideFollowNav(this, options);
    sideNavObj.sideNavScroll();
    sideNavObj.sideNavHover();
    sideNavObj.navBackTop();
    sideNavObj.sideNavLocation();
    return this;
  }
})(jQuery, window, document);