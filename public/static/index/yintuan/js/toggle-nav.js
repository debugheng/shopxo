/**
 * Created by zzf on 2017/8/4.
 */
/*
 * **参数配置说明 **
   "toggleSpeed":300,//展开和收起二级导航的速度
   "unfoldTogether":false,//是否允许同时展开，默认不同时
   "navText":"展开",//默认一级导航文本或者按钮的文本，可选。不填则不变化
   "unfoldNavText":"收起", //配合navText使用，展开后的文本
   "unfoldState":"view-btn-cur",//作为展开后的样式class，同事也是展开后的flag；
   "secondNav":".join-us-cnt"//二级导航的列表项
 */

;(function ($,window,document,undefined) {
  //定义toggleNav的构造函数
  var toggleNav=function(ele,opt){
    this.$element=ele;
    this.defaults={
      "toggleSpeed":300,//展开和收起二级导航的速度
      "unfoldTogether":false,//是否允许同时展开，默认不同时
      // "navText":"展开",
      // "unfoldNavText":"收起"
    }
    this.option=$.extend({},this.defaults,opt);//配置选项
  }
  //定义toggleNav的方法
  toggleNav.prototype={
    //导航单击
    toggleNavClick:function(){
      var thisObj=this,
          unfoldClass=thisObj.option.unfoldState,
          animationSpeed=thisObj.option.toggleSpeed,
          _navText=thisObj.option.navText,
          _unfoldNavText=thisObj.option.unfoldNavText;
          $secondNavList=$(thisObj.option.secondNav);//获取二级导航列表项
      this.$element.each(function (index) {

        $(this).on("click",function () {
          var $index=index,
              $this=$(this);
          if(thisObj.option.unfoldTogether){//允许同时展开多个二级菜单
              if($this.hasClass(unfoldClass)){ //已经展开
                $this.removeClass(unfoldClass).text(_navText);
                $secondNavList.eq($index).slideUp(animationSpeed);
              }
              else{//未展开
                $this.addClass(unfoldClass).text(_unfoldNavText);
                $secondNavList.eq($index).slideDown(animationSpeed);
              }
            }
            else{//不允许同时展开多个二级菜单
            if($this.hasClass(unfoldClass)){ //已经展开
              $this.removeClass(unfoldClass).text(_navText);
              $secondNavList.eq($index).slideUp(animationSpeed);
            }
            else{//未展开
              thisObj.$element.removeClass(unfoldClass).text(_navText);;
              $this.addClass(unfoldClass).text(_unfoldNavText);
              $secondNavList.slideUp(animationSpeed);
              $secondNavList.eq($index).slideDown(animationSpeed);
            }
            }

        })
      })
  }
  };
  $.fn.toggleNavSet=function (options) {
    var toggleNavObj=new toggleNav(this,options);
    toggleNavObj.toggleNavClick();
    return this;
  }
})(jQuery,window,document)