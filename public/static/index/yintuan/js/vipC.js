// JavaScript Document

$(function(){
	//mypoint
	$(".myPoint_item li").click(function(){
		if($(this).find("div").hasClass("myPoint_item_underline")){
			return;
		}else{
			$(".myPoint_item li").find("div").removeClass("myPoint_item_underline");
			$(this).find("div").addClass("myPoint_item_underline");
		}
	})
	
	//myCoupon
	$(".myCoupon_item li").click(function(){
		if($(this).find("div").hasClass("myCoupon_item_underline")){
			return;
		}else{
			$(".myCoupon_item li").find("div").removeClass("myCoupon_item_underline");
			$(this).find("div").addClass("myCoupon_item_underline");
		}
	})
	//information
	$(".information_item li").click(function(){
		if($(this).find("div").hasClass("information_item_underline")){
			return;
		}else{
			$(".information_item li").find("div").removeClass("information_item_underline");
			$(this).find("div").addClass("information_item_underline");
		}
		if($(this).index()==0){
			$(".information_head_pic").hide();
			$(".information_dl").show()
		}else{
			$(".information_dl").hide();
			$(".information_head_pic").show();
		}
	})
	//message 消息管理
  $(".record-item-cnt").hide();
	$(".view-btn").on("click",function () {
		$(this).parents(".record-item-title").addClass("msg-read");
  })

	//消息数量
$(".badge").each(function () {
  var $recordNumVal=parseInt($(this).text()),
      intText = /^\d+$/g;
  if ($recordNumVal == "" || !intText.test($recordNumVal)) {
    $(this).hide();
  }
  else {
    $(this).show();
  }
});

  $(".badge").bind('DOMNodeInserted', function (e) {
    var $recordNumVal=parseInt($(e.target).text()),
        intText = /^\d+$/g;
    if ($recordNumVal == "" || !intText.test($recordNumVal)) {
      $(e.target).hide();
    }
    else {
      $(e.target).show();
    }
  });
})
